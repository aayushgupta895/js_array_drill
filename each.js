
function each(elements, cb) {
  if (!Array.isArray(elements)) {
    throw new Error("elements is not a array");
  }
  if (typeof cb === "function") {
    for (let index = 0; index < elements.length; index++) {
      cb(elements[index], index, elements);
    }
  } else {
    console.log(`second argument passed is not a function`);
    return;
  }
}


module.exports = each;