const filter = require("./filter");

const items = [1, 2, 3, 4, 5, 5];

const res = filter(items, (element, index, elements) => {
  return element % 2 == 0;
});

console.log(res);