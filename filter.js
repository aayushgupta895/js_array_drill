
function filter(elements, cb) {
  if (!Array.isArray(elements)) {
    throw new Error("elements is not a array");
  }
  const array = [];
  for (let index = 0; index < elements.length; index++) {
    const isTrue = cb(elements[index], index, elements);
    if (isTrue) {
      array.push(elements[index]);
    }
  }
  return array;
}

module.exports = filter;