function flatten(nestedArray) {
  const list = [];
  if (Array.isArray(nestedArray)) {
    for (let array of nestedArray) {
      if (Array.isArray(array)) {
        list.push(...flatten(array));
      } else {
        list.push(array);
      }
    }
  }
  return list;
}

module.exports = flatten;