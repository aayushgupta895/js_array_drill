
function find(elements, cb) {
  if (!Array.isArray(elements)) {
    throw new Error("elements is not a array");
  }
  for (let index = 0; index < elements.length; index++) {
    const isTrue = cb(elements[index], index, elements);
    if (isTrue) {
      return elements[index];
    }
  }
  return;
}

module.exports = find;