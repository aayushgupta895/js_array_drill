
const find = require('./find');

const items = [1, 2, 3, 4, 5, 5];

const isAvailable = find(items, (element, index, array) => {
  return element == 4;
});

if(isAvailable){
  console.log(isAvailable, `is available`);
}else{
  console.log(isAvailable, `not available`);
}
