
function map(elements, cb) {
  if (!Array.isArray(elements)) {
    throw new Error("elements is not a array");
  }
  const array = [];
  if (typeof cb === "function") {
    for (let index = 0; index < elements.length; index++) {
      const ans = cb(elements[index], index, elements);
      array.push(ans);
    }
  } else {
    console.log(`second argument passed is not a function`);
    return;
  }
  return array;
}

module.exports = map;