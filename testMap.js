const map = require('./map');

const items = [1, 2, 3, 4, 5, 5];

const result = map(items, (element, index, elements) => {
  return 2 * element;
});

console.log(result);
