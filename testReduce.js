const reduce = require('./reduce');

const items = [1, 2, 3, 4, 5, 5];

const red = reduce(
  items,
  (accumulate, current) => {
    
    return accumulate + current;
  },
  items[0]
);

console.log(red);
