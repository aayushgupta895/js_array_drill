function reduce(elements, cb, startingValue) {
  if (!Array.isArray(elements)) {
    throw new Error("elements is not a array");
  }
  for (let key of elements) {
    startingValue = cb(startingValue, key);
  }
  return startingValue;
}

module.exports = reduce;